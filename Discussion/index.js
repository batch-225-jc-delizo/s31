// Node.js Introduction

// Use the "require" directive load Node.js modules
// The "http modeule" lets Node.js transfer data using the Hyper Text Transfer Protocol(HTTP)
// HTTP is a protocol that allows the fetching of resources such as HTML documents.
// The message sent by the "client", usually a Web browser, called "request"
// The message sent by the "server" as an answer are called "responses"

let http = require("http");

// using this module's createServer() method, we can create an HTTP server that listen to request on a specified port and gives responses back to client.
// A port is a virtual point where a network connections start and end. Each port is associated with a specific process or server

// http.createServer(function(request, response) {

// 	// Use the writeHead() method to:
// 	// Set a status code for the response - a 200 means OK
// 	// Set the content-type of the response as a plain text message

// 	response.writeHead(200, {'Content-Type' : 'text/plain'});

// 	// send the response with text content:
// 	response.end("Welcome to my Page");

// 	// the server will be assigned to port 4000 via the "listen(4000)" method where server will listen to any request that are sent to it eventually communicating with our server.

// }).listen(4000)

// console.log('Server running at localhost:4000');

// =======================================================
// Part 2 with conditionals

const server = http.createServer((request, response) => {

	if (request.url == '/greeting') {

		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end("Here at SM")
	} else if (request.url == '/we-got-it-all-for-you') {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end("We got it all for you");
	} else {
		response.writeHead(404, { 'Content-Type': 'text/plain' });
		response.end("Wa kang pera");
	}
}).listen(4000);

console.log('Server is running at localhost:4000');